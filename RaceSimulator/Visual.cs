﻿using Controller;
using Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaceSimulator
{
    public static class Visual
    {
        public static Direction CurrentDirection { get; set; }

        public static int xPos;
        public static int yPos;
        public enum Direction
        {
            N = 0, E = 1, S = 2, W = 3
        }

        #region graphics
        private static readonly string[] FinishHorizontal = {
            "───────", 
            "  # L  ",
            "  # R  ",
            "───────" };
        private static readonly string[] StartGridHorizontal = {
            "───────", 
            "   L]  ", 
            "  R]   ",
            "───────" };

        private static readonly string[] StraightHorizontal = {
            "───────", 
            "    L  ", 
            "  R    ",
            "───────" };
        private static readonly string[] StraightVertical = { 
            "│     │", 
            "│   R │", 
            "│ L   │", 
            "│     │" };

        private static readonly string[] CornerTopLeft = {
            "┌──────",
            "│ L    ",
            "│   R  ",
            "│     ┌" };
        private static readonly string[] CornerTopRight = {
            @"──────┐", 
            @"   L  │",
             " R    │",
             "┐     │" };
        private static readonly string[] CornerBottomRight = {
            @"┘     │",
            @"  R   │",
            @"    L │",
            @"──────┘" };
        private static readonly string[] CornerBottomLeft = {
            @"│     └",
            @"│   R  ", 
            @"│ L    ",
            @"└──────" };
        #endregion
        public static void Initialize()
        {
            DrawTrack(Data.CurrentRace.Track);
        
        }

        public static void DrawTrack(Track Track)
        {
            //Console.WriteLine(Direction.N + 1);
            CurrentDirection = Direction.E;
            xPos = 20;
            yPos = 10;
            //Console.WriteLine(Data.CurrentRace.ToString());
            // Console.SetCursorPosition(xPos, yPos);




            foreach (Section Section in Track.Sections)
            {
                /*Console.WriteLine($"Printing section string for section: {Section.SectionType}");
                Console.SetCursorPosition(10, 10);
                Console.WriteLine("Setting cursor pos");

                Console.WriteLine(getSectionString(Section).ToString());*/
                //Console.Write(getSectionString(Section));
                PrintSectionString(Section);


            }
        }

        public static void Drawparticipant(IParticipant participant)
        {

        }

        private static void UpdateCursor()
        {
            switch (CurrentDirection)
            {
                case Direction.E:
                    xPos += 7;
                    break;
                case Direction.N:
                    yPos -= 4;
                    break;
                case Direction.S:
                    yPos += 4;
                    break;
                case Direction.W:
                    xPos -= 7;
                    break;
            }
        }


        private static string[] getSectionString(Section Section)
        {
            //Console.WriteLine($"Checking for Section: {Section.SectionType}");
            bool vertical = CurrentDirection == Direction.N || CurrentDirection == Direction.S;
            switch (Section.SectionType)
            {
                case SectionTypes.Finish:
                    return FinishHorizontal;
                case SectionTypes.StartGrid:
                    return StartGridHorizontal;
                case SectionTypes.Straight:
                    //Console.WriteLine("Straight!");
                    return (vertical ? StraightVertical : StraightHorizontal);
                case SectionTypes.LeftCorner:
                    switch (CurrentDirection)
                    {
                        default:
                        case Direction.N:
                            return CornerTopRight;
                        case Direction.E:
                            return CornerBottomRight;
                        case Direction.S:
                            return CornerBottomLeft;
                        case Direction.W:
                            return CornerTopLeft;
                    }
                case SectionTypes.RightCorner:
                    {
                        switch(CurrentDirection)
                        {
                            default:
                            case Direction.N:
                                return CornerTopLeft;
                            case Direction.E:
                                return CornerTopRight;
                            case Direction.S:
                                return CornerBottomRight;
                            case Direction.W:
                                return CornerBottomLeft;
                        }
                    }
            }
            return null;
        }

        private static Direction getNextDirection(Section CurSection)
        {
            switch(CurSection.SectionType)
            {
                default:
                case SectionTypes.Straight:
                    return CurrentDirection;
                case SectionTypes.StartGrid:
                    return Direction.E;
                case SectionTypes.LeftCorner:
                    return (Direction)(((int)CurrentDirection + 3) % 4);
                case SectionTypes.RightCorner:
                    return (Direction)(((int)CurrentDirection + 1) % 4);
            }
        }

        private static void PrintSectionString(Section Section)
        {
            String[] StringSec = getSectionString(Section);
            SectionData sectionData = Data.CurrentRace.getSectionData(Section);
           
            int tempY = yPos;
            foreach (String line in StringSec)
            {
                Console.SetCursorPosition(xPos, tempY);
                Console.Write(line
                    .Replace('L', sectionData.Left?.Name[0] ?? ' ')
                    .Replace('R', sectionData.Right?.Name[0] ?? ' '));
                tempY++;
            }

            CurrentDirection = getNextDirection(Section);
            

            UpdateCursor();

        }
    }
}