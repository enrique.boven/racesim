﻿using Model;
using RaceSimulator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controller
{
    public class Race
    {
        public Race(Track Track, List<IParticipant> Participants)
        {
            this.Track = Track;
            this.Participants = Participants;
            this._random = new Random(DateTime.Now.Millisecond);
            this._positions = new Dictionary<Section, SectionData>();
            SetStartParticipants(Track, Participants);
        }

        public void SetStartParticipants(Track Track, List<IParticipant> Participants)
        {
            List<IParticipant> participantList = new List<IParticipant>(Participants);
            foreach (Section Section in Track.Sections.Reverse()) {
                if(Section.SectionType == SectionTypes.StartGrid)
                {
                    SectionData SectionData = getSectionData(Section);

                    if (participantList.Count > 0)
                    {
                        if (SectionData.Left == null)
                        {
                            SectionData.Left = RandomParticipant(participantList);
                        }
                        if (participantList.Count == 0) break;
                        if (SectionData.Right == null)
                        {
                            SectionData.Right = RandomParticipant(participantList);
                        }
                    }
                }
            }
        }

        public IParticipant RandomParticipant(List<IParticipant> Participants)
        {
            int count = _random.Next(Participants.Count);
            IParticipant participant = Participants[count];
            Participants.Remove(participant);
            return participant;

        }

        public void RandomizeEquipment()
        {
            foreach (IParticipant Participant in Participants)
            {
                Participant.IEquipment.Quality = _random.Next(1, 100);
                Participant.IEquipment.Performance = _random.Next(1, 100);
            }
        }
        public SectionData getSectionData(Section Section)
        {
            if (_positions.ContainsKey(Section)) {
                return _positions[Section];
            }
            else
            {
                _positions.Add(Section, new SectionData());
                return _positions[Section];
            }
        }

        public Track Track { get; set; }
        public List<IParticipant> Participants { get; set; }
        public DateTime StartTime { get; set; }
        private Random _random { get; set; }

        private Dictionary<Section, SectionData> _positions;

    }
}
