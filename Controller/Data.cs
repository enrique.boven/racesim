﻿using Model;
using RaceSimulator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Controller
{
    public static class Data
    {

        public static void Initialize()
        {
            Competition = new Competition();
            AddParticipants();
            AddTracks();
            CurrentRace = new Race(Competition.NextTrack(), Competition.Participants);
        }

        public static void AddParticipants()
        {
            Competition.Participants.Add(new Driver(
                "Bob", 
                10, 
                new Car(9, 10, 50, false), TeamColors.Red));
            Competition.Participants.Add(new Driver(
                "Henry",
                10,
                new Car(9, 12, 40, false), TeamColors.Blue));
            Competition.Participants.Add(new Driver(
                "Krijn",
                10,
                new Car(9, 12, 40, false), TeamColors.Blue));
        }

        public static void AddTracks()
        {
            Competition.Tracks.Enqueue(new Track(
                "Baan Test",
                new SectionTypes[] {
                    SectionTypes.StartGrid,
                    SectionTypes.StartGrid,
                    SectionTypes.Finish,
                    SectionTypes.Straight,
                    SectionTypes.Straight,
                    SectionTypes.RightCorner,
                    SectionTypes.RightCorner,
                    SectionTypes.LeftCorner,
                    SectionTypes.Straight,
                    SectionTypes.RightCorner,
                    SectionTypes.Straight,
                    SectionTypes.Straight,
                    SectionTypes.Straight,
                    SectionTypes.Straight,
                    SectionTypes.RightCorner,
                    SectionTypes.Straight,
                    SectionTypes.Straight,
                    SectionTypes.RightCorner
                    
                }));
            Competition.Tracks.Enqueue(new Track(
                "Baan 1",
                new SectionTypes[] { SectionTypes.Straight, SectionTypes.RightCorner, SectionTypes.Straight, SectionTypes.RightCorner, SectionTypes.Straight, SectionTypes.LeftCorner, SectionTypes.Straight, SectionTypes.RightCorner }));
            Competition.Tracks.Enqueue(new Track(
                "Baan 2",
                new SectionTypes[] { SectionTypes.Straight, SectionTypes.LeftCorner, SectionTypes.Straight, SectionTypes.Finish }));
        }
        public static Competition Competition { get; set; }

        public static Race CurrentRace { get; set; }

        public static void NextRace()
        {
            if(Competition.NextTrack != null)
            {
                //Console.WriteLine("Setting next track to " + Competition.NextTrack().Name);
                CurrentRace.Track = Competition.NextTrack();
            }
        }

    }
}
