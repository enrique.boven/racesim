﻿using RaceSimulator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Competition
    {

        public Competition()
        {
            this.Participants = new List<IParticipant>();
            this.Tracks = new Queue<Track>();
        }
        public List<IParticipant> Participants { get; set; }
        public Queue<Track> Tracks { get; set; }

        public Track? NextTrack()
        {
            Console.WriteLine("Track queue count: " + Tracks.Count);
            return Tracks.Count > 0 ? Tracks.Dequeue() : null;
        }
    }
}
