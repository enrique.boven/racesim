﻿using RaceSimulator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Driver : IParticipant
    {

        public Driver(String Name, int Points, IEquipment IEquipment, TeamColors TeamColor)
        {
            this.Name = Name;
            this.Points = Points;
            this.IEquipment = IEquipment;
            this.TeamColor = TeamColor;
        }

        public string Name { get; set; }
        public int Points { get; set; }
        public IEquipment IEquipment { get; set; }
        public TeamColors TeamColor { get; set; }
    }
}
