﻿using RaceSimulator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Car : IEquipment
    {
       public Car(int Quality, int Performance, int Speed, bool IsBroken)
        {
            this.Quality = Quality;
            this.Performance = Performance;
            this.Speed = Speed;
            this.IsBroken = IsBroken;
        }

        public int Quality { get; set; }
        public int Performance { get; set; }
        public int Speed { get; set; }
        public bool IsBroken { get; set; }
    }
}
