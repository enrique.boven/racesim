﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Track
    {
        public Track(string Name, SectionTypes[] Sections)
        {
            this.Name = Name;
            this.Sections = ConvertArrayToLinkedList(Sections);
        }

        public LinkedList<Section> ConvertArrayToLinkedList(SectionTypes[] SectionTypes)
        {
            LinkedList<Section> SectionList = new LinkedList<Section>();
            foreach (SectionTypes SectionType in SectionTypes)
            {
                SectionList.AddLast(new Section(SectionType));
            }
            return SectionList;
        }
        public string Name { get; set; }

        public LinkedList<Section> Sections { get; set; }
    }
}
