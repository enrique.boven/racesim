﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaceSimulator
{
    public interface IParticipant
    {
        public string Name { get; set; }
        public int Points { get; set; }

        public IEquipment IEquipment { get; set; }

        public TeamColors TeamColor { get; set; }
        
    }

    public enum TeamColors
    {
        Red, Green, Yellow, Grey, Blue
    }
}
