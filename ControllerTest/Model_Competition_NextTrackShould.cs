﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControllerTest
{
    [TestFixture]
    public class Model_Competition_NextTrackShould
    {
        private Competition _competition;

        [SetUp]
        public void SetUp()
        {
            _competition = new Competition();
        }

        [Test]
        public void NextTrack_EmptyQueue_ReturnNull()
        {

            Track result = _competition.NextTrack();
            Assert.IsNull(result);
        }

        [Test]
        public void NextTrack_OneInQueue_ReturnTrack()
        {
            Track Track = new Track("Test Baan",
                new SectionTypes[] { SectionTypes.Straight, SectionTypes.RightCorner, SectionTypes.Straight, SectionTypes.Finish });
            _competition.Tracks.Enqueue(Track);
            Track result = _competition.NextTrack();
            Assert.AreEqual(Track, result);
        }
        [Test]
        public void NextTrack_OneInQueue_RemoveTrackFromQueue()
        {
            Track Track = new Track("Test Baan",
               new SectionTypes[] { SectionTypes.Straight, SectionTypes.RightCorner, SectionTypes.Straight, SectionTypes.Finish });
            _competition.Tracks.Enqueue(Track);
            Track result = _competition.NextTrack();
            result = _competition.NextTrack();
            Assert.IsNull(result);
        }
        [Test]
        public void NextTrack_TwoInQueue_ReturnNextTrack()
        {
            Track Track1 = new Track("Test Baan 1",
               new SectionTypes[] { SectionTypes.Straight, SectionTypes.RightCorner, SectionTypes.Straight, SectionTypes.Finish });
            Track Track2 = new Track("Test Baan 2",
               new SectionTypes[] { SectionTypes.Straight, SectionTypes.RightCorner, SectionTypes.Straight, SectionTypes.Finish });
            _competition.Tracks.Enqueue(Track1);
            _competition.Tracks.Enqueue(Track2);
            Assert.IsTrue(_competition.NextTrack() == Track1);
            Assert.IsTrue(_competition.NextTrack() == Track2);
        }
    }
}
